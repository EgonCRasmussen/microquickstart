import { Component, NgModule, Directive } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

@Directive({
    selector: 'input[log-directive]',
    host: {
        '(input)': 'onInput($event)'
    }
})
class LogDirective {
    onInput(event) {
        console.log(event.target.value);
    }
}

// Component
@Component({
    selector: 'hello-world',
    template: `<h1>Hello {{ name }}!</h1>
        <input type="text" log-directive #myname/>
        <button (click)='onClick(myname.value)'>Say Hello</button>
        `
})
class AppComponent {
    name: string;

    constructor() {
        this.name = 'Angular';
    }

    onClick(value) {
        console.log("Hello", value);

        this.name = value;
    }
}

// Module
@NgModule({
    imports: [BrowserModule],
    declarations: [AppComponent, LogDirective],
    bootstrap: [AppComponent]
})
export class AppModule { }

// App bootstrap
platformBrowserDynamic().bootstrapModule(AppModule);