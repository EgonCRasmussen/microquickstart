# README #

Den simplest mulige Hello World Angular-konfiguration. Ingen moduler installeres, alt hentes når index.html webbes.


### What is this repository for? ###

* Component
* Module og Boot
* Directive.
* Binding og events

### How do I get set up? ###

Benyt en webserver, f.eks. live-server:
`npm install -g live-server`

Start live-server med kommandoen:
`live-server`
